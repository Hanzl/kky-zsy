# KKY-ZSY
## Overview
This project aims to provide a solution to [Spaceship Titanic Challange](https://www.kaggle.com/competitions/spaceship-titanic) which is available at Kaggle.

The script provides several key components:
1. Data Analysis
2. Data Pre-Preprocessing
3. Model Selection
4. Training and Validation
5. Experiment - View tree-based model weights

Firstly, we take a view the data in the available dataset (feature types, missing values, etc.). The target feature "Transported" is of the boolean type.
Secondly, the train test dataset is pre-processed using the fastai TabularPandas. Here, categorical values are encoded, continuous values are normalized, and for features with missing values, new features is created (is missing / is not missing) and the missing values in the original features are filled by median.

These data are then split into train/val/test sets and are used in several models. Namely, state-of-the-art xgboost or lightgbm classifiers, models based on decision trees, SVM, KNN and so on.
Based on the best validation accuracy, the best model is selected to make prediction on the test set. This prediction is saved for the final submission.
Finally, there is an example visualisation of weights/decision rules of two trained decision-tree-based models.

## Guide 
Python version: 3.11


```
pip install numpy pandas jupyterlab seaborn matplotlib scikit-learn fastai xgboost lightgbm python-graphviz
```

